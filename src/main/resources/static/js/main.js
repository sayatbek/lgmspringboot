'use strict';

var singleUploadForm = document.querySelector('#singleUploadForm');
var singleFileUploadInput = document.querySelector('#singleFileUploadInput');
var singleFileUploadError = document.querySelector('#singleFileUploadError');
var singleFileUploadSuccess = document.querySelector(
    '#singleFileUploadSuccess');
var singleFileUploadGrade = document.querySelector('#singleFileUploadGrade');
var singleFileUploadType = document.querySelector('#singleFileUploadType');

var singleUploadForm1 = document.querySelector('#singleUploadForm1');
var singleFileUploadInput1 = document.querySelector('#singleFileUploadInput1');
var singleFileUploadError1 = document.querySelector('#singleFileUploadError1');
var singleFileUploadSuccess1 = document.querySelector(
    '#singleFileUploadSuccess1');
var singleFileUploadGrade1 = document.querySelector('#singleFileUploadGrade1');
var singleFileUploadType1 = document.querySelector('#singleFileUploadType1');

function uploadSingleFile(file, grade, type) {
  var formData = new FormData();
  formData.append("file", file);
  formData.append("grade", grade);
  formData.append("type", type);

  var submitButton = document.getElementById("submitButton");
  submitButton.disabled = true;

  document.getElementById("progress").innerHTML= "Пожалуйста ждите . . . ";

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/uploadFile");

  xhr.onload = function () {
    console.log(xhr.responseText);
    var response = JSON.parse(xhr.responseText);
    if (xhr.status == 200) {
        singleFileUploadError.style.display = "none";
        singleFileUploadSuccess.innerHTML = "<p>Файл загружен успешно.</p>"
        /*+ "<p>Скачать : <a href='" + response.fileDownloadUri + "' target='_blank'>" + response.fileDownloadUri + "</a></p>";*/
        singleFileUploadSuccess.style.display = "block";
        //window.location.replace("/");
    } else {
      singleFileUploadSuccess.style.display = "none";
      singleFileUploadError.innerHTML = (response && response.message)
          || "Some Error Occurred";
    }
  }

  xhr.send(formData);
}

singleUploadForm.addEventListener('submit', function (event) {
  var grade = singleFileUploadGrade.value;
  var files = singleFileUploadInput.files;
  var type = singleFileUploadType.value;
  if (files.length === 0) {
    singleFileUploadError.innerHTML = "Please select a file";
    singleFileUploadError.style.display = "block";
  }
  uploadSingleFile(files[0], grade, type);
  event.preventDefault();
}, true);

function uploadSingleFile1(file, grade, type) {
  var formData = new FormData();
  formData.append("file", file);
  formData.append("grade", grade);
  formData.append("type", type);

  var submitButton = document.getElementById("submitButton1");
  submitButton.disabled = true;

  document.getElementById("progress1").innerHTML= "Пожалуйста ждите . . . ";

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/uploadFile");

  xhr.onload = function () {
    console.log(xhr.responseText);
    var response = JSON.parse(xhr.responseText);
    if (xhr.status == 200) {
      singleFileUploadError1.style.display = "none";
      singleFileUploadSuccess1.innerHTML = "<p>Файл загружен успешно.</p>"
      /*+ "<p>Скачать : <a href='" + response.fileDownloadUri + "' target='_blank'>" + response.fileDownloadUri + "</a></p>";*/
      singleFileUploadSuccess1.style.display = "block";
      //window.location.replace("/");
    } else {
      singleFileUploadSuccess1.style.display = "none";
      singleFileUploadError1.innerHTML = (response && response.message)
          || "Some Error Occurred";
    }
  }

  xhr.send(formData);
}

singleUploadForm1.addEventListener('submit', function (event) {
  var grade = singleFileUploadGrade1.value;
  var files = singleFileUploadInput1.files;
  var type = singleFileUploadType1.value;
  if (files.length === 0) {
    singleFileUploadError1.innerHTML = "Please select a file";
    singleFileUploadError1.style.display = "block";
  }
  uploadSingleFile1(files[0], grade, type);
  event.preventDefault();
}, true);

function analyzeLgmResultFunction(){
  var gLgm = document.getElementById("selLgm");
  var gradeLgm = gLgm.options[gLgm.selectedIndex].value;

  var gUch = document.getElementById("selUch");
  var gradeUch = gUch.options[gUch.selectedIndex].value;

  document.location.href = "/analyzeLgmResult?lgmId="+gradeLgm+"&uchId="+gradeUch;
}