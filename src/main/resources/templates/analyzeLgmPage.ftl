<!DOCTYPE html>
<html>
<#include "libs.ftl">
<body>
<#include "menu.ftl">
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <label for="sel1">Выберите ЛГМ:</label>
    </div>
    <div class="col-md-4">
      <label for="sel1">Выберите Учебное Пособие:</label>
    </div>
    <div class="col-md-2"></div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <select class="form-control" id="selLgm">
          <#list lgmList as lgm>
            <option value="${lgm.id}"><p class="bg-danger">${lgm.grade}
              класс</p> (${lgm.title})
            </option>
          </#list>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <select class="form-control" id="selUch">
          <#list uchList as uch>
            <option value="${uch.id}"><p class="bg-danger">${uch.grade}
              класс</p> (${uch.title})
            </option>
          </#list>
        </select>
      </div>
    </div>

    <div class="col-md-2">
      <a class="btn btn-large btn-primary"
         onclick="analyzeLgmResultFunction();"
         id="lgmuchanalyze">Готово</a>
    </div>
  </div>
</div>
<#include "js.ftl">
</body>
</html>