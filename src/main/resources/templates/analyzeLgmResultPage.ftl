<!DOCTYPE html>
<html>
<#include "libs.ftl">
<body>
<#include "menu.ftl">
<div class="container">
  <div class="row">
    <div class="col-sm-4">
      <h2>Сравнение</h2>
      <p>Слова из ЛГМ и их частота повторения в учебнике</p>

      <form  class="form-group" method="POST" action="excel" enctype="multipart/form-data" >
        <input type="hidden" value="${gLgm}" name="gLgm" />
        <input type="hidden" value="${gUch}" name="gUch"/>
        <input type="hidden" value="${lgmId}" name="lgmId" />
        <input type="hidden" value="${uchId}" name="uchId"/>
        <input class="btn btn-success" type="submit" value="Выгрузить в эксель" name="uploadMin" id="uploadMin" />
      </form>
    </div>
    <div class="col-sm-6">
      <table class="table">
        <thead>
        <tr>
          <th>${percentageLgmInUch}% слов из ЛГМ встретились Учебнике</th>
        </tr>
        </thead>
        <tbody>
        <tr><td class="center" align="center">
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="${percentageLgmInUch}"
                 aria-valuemin="0" aria-valuemax="100" style="width:${percentageLgmInUch}%">
              ${percentageLgmInUch}%
            </div>
          </div>
        </td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
        <tr>
          <th>${percentageUchInLgm}% из ВСЕХ слов Учебника взято из ЛГМ (с учетом их повторения в Учебнике) </th>
        </tr>
        </thead>
        <tbody>
        <tr><td class="center" align="center">
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="<%=pers %>"
                 aria-valuemin="0" aria-valuemax="100" style="width:${percentageUchInLgm}%">
            ${percentageUchInLgm}%
            </div>
          </div>
        </td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3"><h4>Сорт по алфавиту</h4></div>
    <div class="col-sm-2"><h4></h4></div>
    <#--<div class="col-sm-3"><h4>Сорт по частоте</h4></div>-->
    <div class="col-sm-2"><h4></h4></div>
  </div>
  <div class="row">
    <div class="col-sm-3">
      <table class="table">
        <thead>
        <tr>
          <th>Слова из ЛГМ которые есть в учебнике</th>
          <th>Частота повторения в учебнике</th>
        </tr>
        </thead>
        <tbody>
        <#assign count = 0>
        <#assign countAll = 0>
        <#list lgmTermsList as lgmTerm>
        <#if 0 < lgmTerm.count>
        <#assign count++>
        <#assign countAll = countAll + lgmTerm.count>
        <tr class="active">
          <td><div class="cell bg-success">${lgmTerm.word}</div></td>
          <td><div class="cell bg-success">${lgmTerm.count}</div>
          </td>
        </tr>
        </#if>
        </#list>
        <tr class="active">
          <td><div class="cell bg-warning">Всего: ${count} из ${lgmTermsList?size}</div></td>
          <td><div class="cell bg-warning">${countAll}</div></td>
        </tr>
        </tbody>
      </table>
    </div>

    <div class="col-sm-2">
      <table class="table">
        <thead>
        <tr>
          <th>Слова из ЛГМ <br>которых нет<br> в учебнике</th>
        </tr>
        </thead>
        <tbody>
        <#assign count = 0>
       <#list lgmTermsList as lgmTerm>
       <#if lgmTerm.count == 0>
        <#assign count++>
        <tr class="active">
          <td><div class="cell bg-danger">${lgmTerm.word}</div></td>
        </tr>
       </#if>
       </#list>
        <tr class="active">
          <td><div class="cell bg-warning">Всего: ${count} из ${lgmTermsList?size}</div></td>
        </tr>
        </tbody>
      </table>
    </div>

    <#--<div class="col-sm-3">-->
      <#--<table class="table">-->
        <#--<thead>-->
        <#--<tr>-->
          <#--<th>Слова из ЛГМ которые есть в учебнике</th>-->
          <#--<th>Частота повторения в учебнике</th>-->
        <#--</tr>-->
        <#--</thead>-->
        <#--<tbody>-->
        <#--<%-->
        <#--MyQuickSort sorter = new MyQuickSort();-->
        <#--sorter.sort(lgmNew);-->
        <#--for(Word w : lgmNew){-->
        <#--if(w.count > 0){%>-->
        <#--<tr class="active">-->
          <#--<td><div class="cell bg-success"><%=w.name %></div></td>-->
          <#--<td><div class="cell bg-success"><%=w.count %></div></td>-->
        <#--</tr>-->
        <#--<%-->
        <#--}-->
        <#--}%>-->
        <#--<tr class="active">-->
          <#--<td><div class="cell bg-warning">Всего: <%=allLgm%></div></td>-->
          <#--<td><div class="cell bg-warning"><%=allLgmTotal%></div></td>-->
        <#--</tr>-->
        <#--</tbody>-->
      <#--</table>-->
    <#--</div>-->
  <#--</div>-->
</div>
<#include "js.ftl">
</body>
</html>