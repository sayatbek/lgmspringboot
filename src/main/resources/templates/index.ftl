<!DOCTYPE html>
<html>
<#include "libs.ftl">
<body>
<#include "menu.ftl">
<div class="upload-container">
  <div class="upload-header">
    <h2>Загрузить ЛГМ</h2>
  </div>
  <div class="upload-content">
    <div class="single-upload">
      <form id="singleUploadForm" name="singleUploadForm">
        <input id="singleFileUploadType" type="hidden" name="type" value="lgm"/>
        <br/>
        Класс: <select id="singleFileUploadGrade" type="text" name="grade" value="1" required>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
      </select>
        <br/>
        <input id="singleFileUploadInput" type="file" name="file" class="file-input" required/>
        <button type="submit" id="submitButton" class="primary submit-btn">Загрузить</button>
      </form>
      <div class="upload-response">
        <br/>
        <div id="progress"></div>
        <div id="singleFileUploadError"></div>
        <div id="singleFileUploadSuccess"></div>
      </div>
    </div>
  </div>
</div>

<div class="upload-container">
  <div class="upload-header">
    <h2>Загрузить Учебник</h2>
  </div>
  <div class="upload-content">
    <div class="single-upload">
      <form id="singleUploadForm1" name="singleUploadForm1">
        <input id="singleFileUploadType1" type="hidden" name="type" value="uch"/>
        <br/>
        Класс: <select id="singleFileUploadGrade1" type="text" name="grade" value="1" required>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
      </select>
        <br/>
        <input id="singleFileUploadInput1" type="file" name="file" class="file-input" required/>
        <button type="submit" id="submitButton1" class="primary submit-btn">Загрузить</button>
      </form>
      <div class="upload-response">
        <br/>
        <div id="progress1"></div>
        <div id="singleFileUploadError1"></div>
        <div id="singleFileUploadSuccess1"></div>
      </div>
    </div>
  </div>
</div>
<#include "js.ftl">
<div>
  @Sayatbek
</div>
</body>
</html>