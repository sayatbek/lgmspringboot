package com.istep.lgm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class LgmTerms implements Comparable{

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name = "id", columnDefinition = "serial")
  private int id;

  public LgmTerms(int lgmId, String word){
    this.lgmId = lgmId;
    this.word = word;
  }

  int lgmId;
  String word;

  @ColumnDefault("0")
  int count;

  @Override
  public int compareTo(Object o) {
    return this.word.compareTo(((LgmTerms)o).word);
  }
}
