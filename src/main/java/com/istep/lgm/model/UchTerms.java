package com.istep.lgm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UchTerms {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name = "id", columnDefinition = "serial")
  private int id;

  public UchTerms(int uchId, String word){
    this.uchId = uchId;
    this.word = word;
  }

  int uchId;
  String word;

  @ColumnDefault("0")
  int count;
}
