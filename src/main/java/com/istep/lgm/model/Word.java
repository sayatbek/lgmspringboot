package com.istep.lgm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "terms")
public class Word {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private int id;

  String name;
  String type;
  Integer quantity;
  Double percentage;
}