package com.istep.lgm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "uchebnik",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"grade"})})
public class Uchebnik {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private int id;

  public Uchebnik(String title, String grade){
    this.title = title;
    this.grade = grade;
  }
  String title;

  @Column(unique = true)
  String grade;
}
