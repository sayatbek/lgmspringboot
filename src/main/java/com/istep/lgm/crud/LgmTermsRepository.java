package com.istep.lgm.crud;

import com.istep.lgm.model.LgmTerms;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LgmTermsRepository extends JpaRepository<LgmTerms, Long> {

  List<LgmTerms> findAllByLgmId(int lgmId);

  void removeAllByLgmId(Integer lgmId);
}
