package com.istep.lgm.crud;

import com.istep.lgm.model.Uchebnik;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UchRepository extends JpaRepository<Uchebnik, Long> {

  List<Uchebnik> findAll();

  Uchebnik findAllByGrade(String grade);

  Uchebnik findById(int id);
}
