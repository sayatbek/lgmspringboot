package com.istep.lgm.crud;

import com.istep.lgm.model.Lgm;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LgmRepository extends JpaRepository<Lgm, Long> {

  List<Lgm> findAll();

  Lgm findByGrade(String grade);

  Lgm findById(int id);
}
