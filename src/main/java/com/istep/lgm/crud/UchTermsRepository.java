package com.istep.lgm.crud;

import com.istep.lgm.model.UchTerms;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UchTermsRepository extends JpaRepository<UchTerms, Long> {

  List<UchTerms> findAllByUchId(int uchId);

  void removeAllByUchId(Integer uchId);
}
