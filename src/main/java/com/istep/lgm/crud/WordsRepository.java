package com.istep.lgm.crud;

import com.istep.lgm.model.Word;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordsRepository extends JpaRepository<Word, Long> {

  List<Word> findAll();

  List<Word> findByName(String name);
}
