package com.istep.lgm.service;

import com.istep.lgm.crud.LgmRepository;
import com.istep.lgm.crud.LgmTermsRepository;
import com.istep.lgm.crud.UchRepository;
import com.istep.lgm.crud.UchTermsRepository;
import com.istep.lgm.crud.WordsRepository;
import com.istep.lgm.model.Lgm;
import com.istep.lgm.model.Uchebnik;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
public class ReadFileService {

  @Autowired
  NetworkService networkService;

  @Autowired
  WordsRepository wordsRepository;

  @Autowired
  LgmRepository lgmRepository;

  @Autowired
  UchRepository uchRepository;

  @Autowired
  LgmTermsRepository lgmTermsRepository;

  @Autowired
  UchTermsRepository uchTermsRepository;

  private static SessionFactory sessionFactory
      = HibernateUtility.getSessionFactory();

  @Transactional
  public void parseFile(MultipartFile file, Path fileStorageLocation, String grade, String type) {
    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
    Path filePath = fileStorageLocation.resolve(fileName).normalize();

    try (PDDocument document = PDDocument.load(new File(
        fileStorageLocation.toAbsolutePath().toString() + "/" + StringUtils
            .cleanPath(file.getOriginalFilename())))) {

      document.getClass();

      if (!document.isEncrypted()) {

        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
        stripper.setSortByPosition(true);

        PDFTextStripper tStripper = new PDFTextStripper();

        String pdfFileInText = tStripper.getText(document);
        //System.out.println("Text:" + st);

        // split by whitespace
        String words[] = pdfFileInText.split("\\s+");
        List<String> wordList = new ArrayList<>();
        for (String word : words) {
          word = stripWord(word);
          if (word.length() > 2) {
            wordList.add(word);
          }
        }

        if (type.equals("lgm")) {
          Lgm lgm = lgmRepository.findByGrade(grade);
          if (lgm == null) {
            lgm = new Lgm(fileName, grade);
          } else {
            lgm.setTitle(fileName);
          }
          lgmRepository.save(lgm);

          String queryStr = "insert into lgm_terms(lgm_id, word) values";
          lgmTermsRepository.removeAllByLgmId(lgm.getId());
          // List<LgmTerms> lgmTermsList = new ArrayList<>();
          Iterator<String> it = wordList.iterator();
          while (it.hasNext()) {
            String word = it.next();
            //LgmTerms lgmTerms = new LgmTerms(lgm.getId(), word);
            //lgmTermsList.add(lgmTerms);
            if (it.hasNext()) {
              queryStr += "(" + lgm.getId() + ",'" + word + "'),";
            } else {
              queryStr += "(" + lgm.getId() + ",'" + word + "');";
            }
          }

          executeQuery(queryStr);

          //System.out.println(queryStr);
          //lgmTermsRepository.saveAll(lgmTermsList);
          //System.out.println("Id: "+lgm.getId());
        } else {
          Uchebnik uchebnik = uchRepository.findAllByGrade(grade);
          if (uchebnik == null) {
            uchebnik = new Uchebnik(fileName, grade);
          } else {
            uchebnik.setTitle(fileName);
          }
          uchRepository.save(uchebnik);

          String queryStr = "insert into uch_terms(uch_id, word) values";
          lgmTermsRepository.removeAllByLgmId(uchebnik.getId());
          //List<UchTerms> uchTermsList = new ArrayList<>();
          Iterator<String> it = wordList.iterator();
          while (it.hasNext()) {
            String word = it.next();
            //UchTerms uchTerms = new UchTerms(uchebnik.getId(), word);
            //uchTermsList.add(uchTerms);
            if (it.hasNext()) {
              queryStr += "(" + uchebnik.getId() + ",'" + word + "'),";
            } else {
              queryStr += "(" + uchebnik.getId() + ",'" + word + "');";
            }
          }

          executeQuery(queryStr);
//          String data = (new Gson()).toJson(wordList);
//          String stemmed = networkService
//              .curlJson("http://35.198.162.25:8585/api/stemlist", data, "POST");
//
//          List<String> terms = new Gson().fromJson(stemmed, new TypeToken<List<String>>() {
//          }.getType());
        }
      }

    } catch (InvalidPasswordException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Transactional
  public void executeQuery(String queryStr) {
    Session session = sessionFactory.openSession();
    session.beginTransaction();
    SQLQuery insertQuery = session.createSQLQuery(queryStr);
    insertQuery.executeUpdate();
    session.getTransaction().commit();
    session.close();
  }

  public static String stripWord(String word) {
    word = word.replaceAll("i", "і");
    word = word.replaceAll("ї", "і");
    word = word.replaceAll("a", "а");
    word = word.replaceAll("b", "б");
    word = word.replaceAll("є", "қ");
    word = word.replaceAll("ііі", "ш");
    word = word.replaceAll("¥", "ұ");
    word = word.replaceAll("ӛ", "ө");
    word = word.replaceAll("ё", "е");

    word = word
        .replaceAll("(?!-)\\p{Punct}|\\p{IsDigit}|\\^-|x|v|l|«|»||ї|\\s|…|©|№|[a-zA-Z]|—|¹|”|“|•",
            "");
    try {
      for (int j = 0; j < 5; j++) {
        if (word.length() > 2) {
          if (word.charAt(word.length() - 1) == '-' || word.charAt(word.length() - 1) == '–') {
            word = word.substring(0, word.length() - 1);
          }
          if (word.charAt(0) == '-' || word.charAt(0) == '–') {
            word = word.substring(1, word.length());
          }
        }
      }
    } catch (Exception e) {
    }

    return word.toLowerCase();
  }
}
