package com.istep.lgm.service;

import com.istep.lgm.model.LgmTerms;
import com.istep.lgm.model.UchTerms;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class AnalyzeService {

  public int getLgmInUchPercentage(List<LgmTerms> lgmTermsList, List<UchTerms> uchTermsList) {
    int counter = 0;
    for (LgmTerms lgmTerm : lgmTermsList) {
      boolean found = Boolean.FALSE;
      for (UchTerms uchTerm : uchTermsList) {
        if (isSimilar(lgmTerm, uchTerm)) {
          found = Boolean.TRUE;
          lgmTerm.setCount(lgmTerm.getCount()+1);
          //break;
        }
      }
      if (found) {
        counter++;
      }
    }
    return (counter * 100) / lgmTermsList.size();
  }

  public int getUchInLgmPercentage(List<LgmTerms> lgmTermsList, List<UchTerms> uchTermsList) {
    int counter = 0;
    for(UchTerms uchTerm : uchTermsList){
      boolean found = Boolean.FALSE;
      for(LgmTerms lgmTerm : lgmTermsList){
        if(isSimilar(lgmTerm, uchTerm)){
          found = Boolean.TRUE;
          break;
        }
      }
      if(found){
        counter++;
      }
    }
    return (counter*100)/uchTermsList.size();
  }

  public boolean isSimilar(LgmTerms lgmTerm, UchTerms uchTerm){
    if(uchTerm.getWord().length() < lgmTerm.getWord().length()){
      return false;
    }
    if(!uchTerm.getWord().contains(lgmTerm.getWord())){
      return false;
    }
    for(int i=0;i<lgmTerm.getWord().length(); i++){
      if(lgmTerm.getWord().charAt(i) != uchTerm.getWord().charAt(i)){
        return false;
      }
    }
    return true;
  }
}
