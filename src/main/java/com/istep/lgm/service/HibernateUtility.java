package com.istep.lgm.service;

import java.io.File;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class HibernateUtility {

  private static final SessionFactory sessionFactory = buildSessionFactory();

  @Autowired
  private static ApplicationContext context;

  private static SessionFactory buildSessionFactory() {
    File file = new File("/etc/hibernate.cfg.xml");

    Configuration configuration = new Configuration();
    configuration.configure(file);

    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
        .applySettings(configuration.getProperties()).build();

    SessionFactory sessionFactory = configuration
        .buildSessionFactory(serviceRegistry);

    return sessionFactory;
  }

  public static SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}