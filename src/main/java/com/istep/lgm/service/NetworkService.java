package com.istep.lgm.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class NetworkService {

  public String curlJson(String urlStr, String data, String method) {
    String result = "";

    byte[] postData = data.getBytes(StandardCharsets.UTF_8);

    HttpURLConnection con = null;

    try {
      URL url = new URL(urlStr);

      con = (HttpURLConnection) url.openConnection();
      con.setConnectTimeout(10000);
      con.setReadTimeout(10000);
      con.setDoOutput(true);
      con.setRequestMethod(method);
      con.setRequestProperty("User-Agent", "mainsite");
      con.setRequestProperty("Content-Type", "application/json");

      try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
        wr.write(postData);
      }

      if (200 <= con.getResponseCode() && con.getResponseCode() <= 299) {
        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = br.readLine()) != null) {
          response.append(inputLine);
          result += inputLine;
        }
        br.close();
      } else {
        BufferedReader br = new BufferedReader(new InputStreamReader(con.getErrorStream(), "UTF-8"));
      }

    } catch (ProtocolException e) {
      e.printStackTrace();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (con != null) {
        con.disconnect();
      }
    }
    return result;
  }

  public String curlUrl(String urlStr, String word) {
    String result = "";

    try {
      URI uri = new URI(
          "http",
          "35.198.162.25:8585",
          "/api/stem",
          "word=" + word,
          null);
      String request = uri.toASCIIString();

      URL obj = new URL(request);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();
      con.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");

      // optional default is GET
      con.setRequestMethod("GET");

      int responseCode = con.getResponseCode();
      //System.out.println("\nSending 'GET' request to URL : " + urlStr);
      //System.out.println("Response Code : " + responseCode);

      BufferedReader in = new BufferedReader(
          new InputStreamReader(con.getInputStream(), "UTF-8"));
      String inputLine;
      StringBuffer response = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
        result += inputLine;
      }
      in.close();

      //print result
      //System.out.println(response.toString());
    } catch (ProtocolException e) {
      e.printStackTrace();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }

    return result;
  }

  URI applyParameters(URI baseUri, String[] urlParameters) {
    StringBuilder query = new StringBuilder();
    boolean first = true;
    for (int i = 0; i < urlParameters.length; i += 2) {
      if (first) {
        first = false;
      } else {
        query.append("&");
      }
      try {
        query.append(urlParameters[i]).append("=")
            .append(URLEncoder.encode(urlParameters[i + 1], "UTF-8"));
      } catch (UnsupportedEncodingException ex) {
        /* As URLEncoder are always correct, this exception
         * should never be thrown. */
        throw new RuntimeException(ex);
      }
    }
    try {
      return new URI(baseUri.getScheme(), baseUri.getAuthority(),
          baseUri.getPath(), query.toString(), null);
    } catch (URISyntaxException ex) {
      /* As baseUri and query are correct, this exception
       * should never be thrown. */
      throw new RuntimeException(ex);
    }
  }
}
