package com.istep.lgm.controller;

import com.istep.lgm.crud.LgmRepository;
import com.istep.lgm.crud.LgmTermsRepository;
import com.istep.lgm.crud.UchRepository;
import com.istep.lgm.crud.UchTermsRepository;
import com.istep.lgm.model.Lgm;
import com.istep.lgm.model.LgmTerms;
import com.istep.lgm.model.UchTerms;
import com.istep.lgm.model.Uchebnik;
import com.istep.lgm.service.AnalyzeService;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

  private final Logger logger = LoggerFactory.getLogger(IndexController.class);

  @Autowired
  AnalyzeService analyzeService;

  @Autowired
  LgmRepository lgmRepository;

  @Autowired
  UchRepository uchRepository;

  @Autowired
  LgmTermsRepository lgmTermsRepository;

  @Autowired
  UchTermsRepository uchTermsRepository;

  @RequestMapping("/")
  String home(ModelMap modal) {
    return "index";
  }

  @RequestMapping("/uploadFile")
  String uploadFile(ModelMap modal) {
    return "uploadFile";
  }

  @RequestMapping("/analyzeLgm")
  String analyzeLgm(ModelMap modal) {
    modal.addAttribute("lgmList", lgmRepository.findAll());
    modal.addAttribute("uchList", uchRepository.findAll());
    return "analyzeLgmPage";
  }

  @RequestMapping(value = "/analyzeLgmResult", method = RequestMethod.GET)
  String analyzeLgmResult(ModelMap modal, @RequestParam("lgmId") String lgmId,
      @RequestParam("uchId") String uchId) {
    List<UchTerms> uchTermsList = uchTermsRepository.findAllByUchId(Integer.parseInt(uchId));
    List<LgmTerms> lgmTermsList = lgmTermsRepository.findAllByLgmId(Integer.parseInt(lgmId));

    Lgm lgm = lgmRepository.findById(Integer.parseInt(lgmId));
    Uchebnik uch = uchRepository.findById(Integer.parseInt(uchId));

    Collections.sort(lgmTermsList);
    modal.addAttribute("lgmId", lgmId);
    modal.addAttribute("uchId", uchId);
    modal.addAttribute("gLgm", lgm.getGrade());
    modal.addAttribute("gUch", uch.getGrade());
    modal.addAttribute("percentageLgmInUch", analyzeService.getLgmInUchPercentage(lgmTermsList, uchTermsList));
    modal.addAttribute("percentageUchInLgm", analyzeService.getUchInLgmPercentage(lgmTermsList, uchTermsList));
    modal.addAttribute("lgmTermsList", lgmTermsList);
    return "analyzeLgmResultPage";
  }

}
