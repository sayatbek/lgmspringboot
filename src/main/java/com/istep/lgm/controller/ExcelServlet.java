package com.istep.lgm.controller;

import com.istep.lgm.crud.LgmTermsRepository;
import com.istep.lgm.crud.UchTermsRepository;
import com.istep.lgm.model.LgmTerms;
import com.istep.lgm.model.UchTerms;
import com.istep.lgm.service.AnalyzeService;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExcelServlet extends HttpServlet {

  @Autowired
  LgmTermsRepository lgmTermsRepository;

  @Autowired
  UchTermsRepository uchTermsRepository;

  @Autowired
  AnalyzeService analyzeService;

  @RequestMapping(value = "/excel")
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");

    String gLgm = request.getParameter("gLgm");
    String gUch = request.getParameter("gUch");

    String lgmId = request.getParameter("lgmId");
    String uchId = request.getParameter("uchId");

    List<UchTerms> uchTermsList = uchTermsRepository.findAllByUchId(Integer.parseInt(uchId));
    List<LgmTerms> lgmTermsList = lgmTermsRepository.findAllByLgmId(Integer.parseInt(lgmId));

    int allLgm = 0;
    for (LgmTerms l : lgmTermsList) {
      boolean bil = false;
      for (UchTerms w : uchTermsList) {
        if (analyzeService.isSimilar(l, w)) {
          l.setCount(l.getCount() + 1);
          if (!bil) {
            allLgm++;
            bil = true;
          }
        }
      }
    }

    int allUch = 0;
    for (UchTerms w : uchTermsList) {
      for (LgmTerms l : lgmTermsList) {
        if (analyzeService.isSimilar(l, w)) {
          allUch++;
          break;
        }
      }
    }

    HSSFWorkbook doc = new HSSFWorkbook();
    HSSFSheet sheet = doc
        .createSheet("ЛГМ за " + gLgm + " класс на Учебник " + gUch + " за 5 класс");

    CellStyle cs = doc.createCellStyle();
    cs.setWrapText(true);

    int rowCounter = 0;
    int cols = 0;
    HSSFRow row = sheet.createRow(rowCounter);
    rowCounter++;

    HSSFCell cell0 = row.createCell(cols++);
    cell0.setCellValue("Слова из ЛГМ  которые есть в учебнике");

    HSSFCell cell1 = row.createCell(cols++);
    cell1.setCellValue("Частота повторения в учебнике");

    HSSFCell cell2 = row.createCell(cols++);
    cell2.setCellValue("Слова из ЛГМ которых нет в учебнике");

    HSSFCell cell5 = row.createCell(cols++);
    cell5.setCellValue("% слов из ЛГМ которые присутствуют Учебнике");

    HSSFCell cell6 = row.createCell(cols++);
    cell6.setCellValue("% ВСЕХ слов Учебника взятых из ЛГМ (с учетом их повторения в Учебнике)");

    HSSFFont font = doc.createFont();
    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

    cs.setFont(font);

    cell0.setCellStyle(cs);
    cell1.setCellStyle(cs);
    cell2.setCellStyle(cs);
    cell5.setCellStyle(cs);
    cell6.setCellStyle(cs);

    int popal = 0;
    int popal1 = 0;
    for (LgmTerms l : lgmTermsList) {
      if (l.getCount() > 0) {
        HSSFRow r = sheet.createRow(rowCounter);
        r.createCell(0).setCellValue(l.getWord());
        r.createCell(1).setCellValue(l.getCount());
        rowCounter++;
        popal++;
        popal1 += l.getCount();
      }
    }
    HSSFRow rw = sheet.createRow(rowCounter);
    rw.createCell(0).setCellValue("Всего: " + popal + " из " + lgmTermsList.size());
    rw.createCell(1).setCellValue(popal1);

    rowCounter = 1;
    for (LgmTerms l : lgmTermsList) {
      if (l.getCount() == 0) {
        HSSFRow r = sheet.getRow(rowCounter);
        if (r == null) {
          r = sheet.createRow(rowCounter);
        }
        r.createCell(2).setCellValue(l.getWord());
        rowCounter++;
      }
    }
    HSSFRow rw1 = sheet.getRow(rowCounter);
    if (rw1 == null) {
      rw1 = sheet.createRow(rowCounter);
    }
    rw1.createCell(2).setCellValue("Всего: " + (rowCounter - 1) + " из " + lgmTermsList.size());

    int pers = (popal * 100) / lgmTermsList.size();

    HSSFRow rp1 = sheet.getRow(1);
    if (rp1 == null) {
      rp1 = sheet.createRow(1);
    }
    rp1.createCell(3)
        .setCellValue(pers + "%");

    pers = allUch * 100 / uchTermsList.size();
    rp1.createCell(4)
        .setCellValue(pers + "%");

    //write it as an excel attachment
    ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
    doc.write(outByteStream);
    byte[] outArray = outByteStream.toByteArray();
    response.setContentType("application/ms-excel");
    response.setContentLength(outArray.length);
    response.setHeader("Expires:", "0"); // eliminates browser caching
    response.setHeader("Content-Disposition", "attachment; filename=testxls.xls");
    OutputStream outStream = response.getOutputStream();
    outStream.write(outArray);
    outStream.flush();
  }
}
