#Author

@Sayatbek
ISTEP Solutions
Almaty, Kazakhstan
2018

#Clone the source code

Go to:
https://bitbucket.org/sayatbek/lgmspringboot

or run this bash command:

```
$ git clone https://sayatbek@bitbucket.org/sayatbek/lgmspringboot.git
```

## LGM Analyzer Project
## Spring Boot File Upload / Download / Analyze / Compare Rest API Example

**Tutorial**: [Lgm analyzer with Spring Boot](http://ec2-35-165-39-175.us-west-2.compute.amazonaws.com:8000)

## Steps to Setup

** Run the app using maven**

```bash
mvn spring-boot:run
```

That's it! The application can be accessed at `http://localhost:8080`.

You may also package the application in the form of a jar and then run the jar file like so -

```bash
mvn clean package
java -jar target/lgm-analyzer-0.0.1-VERSION.jar
```

##requires
 hibernate.cfg.xml file in the /etc/  directory